# Named entity recognition from the NEREL dataset / pd_d

## Comparison

**Comparison models. natasha, spacy, stanza with nerel. (A).**

```
hse_fp_nerel_read_and_test_3.ipynb
```

**Comparison with Constructed Pyramid Model. Sources are located. (A).**

```
hse_fp_nerel_read_and_test_o_tp_1.ipynb
```

**Comparison with Constructed Pyramid Model with Double Toi. Sources are located. (A).**

```
hse_fp_pyramid_d_tp_e_1.ipynb
```

**Other sources.**

```
Comparison with pyramid model. Using https://gitlab.com/kartzum/pd without changes.
hse_fp_nerel_read_and_test_3_pd_1.ipynb
```

## Training

**Train Constructed Pyramid Model with Training Process. Sources are located. (A).**

```
hse_fp_pyramid_o_tp_1.ipynb
```

**Train Constructed Pyramid Model with Double Toi Training Process. Sources are located. (A).**

```
hse_fp_pyramid_d_tp_1.ipynb
```

**Train Original Pyramid Model.**

```
Using: https://gitlab.com/kartzum/pd / https://github.com/LorrinWWW/Pyramid.git
hse_fp_pyramid_x_1.ipynb
```

**Train Constructed Pyramid Model.**

```
hse_fp_pyramid_constructing_1.ipynb
```

**Train Original Pyramid Model with Single.**

```
hse_fp_pyramid_n_2a.ipynb
```

**Train Original Pyramid Model with Double.**

```
hse_fp_pyramid_n_2b.ipynb
```

## Additional experiments

**Constructed Pyramid Model. Sources are located. 7, 10, 15.**

```
hse_fp_nerel_read_and_test_o_tp_1_p1.ipynb
hse_fp_nerel_read_and_test_o_tp_1_p2.ipynb
hse_fp_nerel_read_and_test_o_tp_1_p3.ipynb
```

```
hse_fp_pyramid_o_tp_1_pt.ipynb
```

**Constructed Pyramid Model with Double Toi. Sources are located. 7, 10, 15.**

```
hse_fp_pyramid_d_tp_e_2_p1.ipynb
hse_fp_pyramid_d_tp_e_2_p2.ipynb
hse_fp_pyramid_d_tp_e_2_p3.ipynb
```

## Other Sources

```
* hse_fp_pyramid_r_1.ipynb. Eval Pyramid Model.

* hse_fp_pyramid_exp_1.ipynb. Eval Pyramid Model with Entity Data Provider.

* hse_fp_pyramid_n_1.ipynb. Experiments with pyramid.

* hse_fp_pyramid_n_tokens_1.ipynb. Experiments with pyramid and tokens.
```

## Notes

```
Steps for traning.
(1) https://github.com/nerel-ds/nested-ner-benchmarks / https://github.com/LorrinWWW/Pyramid
(2) json for Pyramid NER
(3) FastText (cc300ru) https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.ru.300.vec.gz
(4) https://github.com/nerel-ds/nested-ner-benchmarks/blob/dev/repos/Pyramid/train-dummy.sh
```

## Resources

* [base paper](https://aclanthology.org/2020.acl-main.525/)
* [base repo](https://github.com/LorrinWWW/Pyramid)
* [data base paper](https://arxiv.org/abs/2108.13112)  
* [data base paper pdf](https://arxiv.org/pdf/2108.13112.pdf)  
* [NEREL](https://gitlab.com/kartzum/d/-/tree/master/NEREL)  
* [NEREL for Pyramid](https://gitlab.com/kartzum/d/-/tree/master/pyramid_d)  
* [nested-ner-benchmarks](https://github.com/nerel-ds/nested-ner-benchmarks)  
* [clone of https://github.com/LorrinWWW/Pyramid with improvements](https://gitlab.com/kartzum/pd)
* [Pyramid with improvements and exp](https://gitlab.com/kartzum/pd_n)  
* [natasha](https://github.com/natasha)
* [natasha.ner](https://github.com/natasha/slovnet#ner)

